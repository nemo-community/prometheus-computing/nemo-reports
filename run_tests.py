#!/usr/bin/env python
""" Run tests
"""
import argparse
import os
import subprocess
import sys

import django
from django.conf import settings
from django.core.management import execute_from_command_line
from django.test.utils import get_runner

if __name__ == "__main__":
    test_settings = "NEMO_reports.tests.test_settings"
    parser = argparse.ArgumentParser()
    parser.add_argument("--billing", nargs="*")
    if parser.parse_args().billing is not None:
        subprocess.call([sys.executable, "-m", "pip", "install", "--upgrade", "NEMO-billing"])
        test_settings = "NEMO_reports.tests.test_settings_with_billing"
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", test_settings)
    execute_from_command_line(["", "migrate"])
    django.setup()
    TestRunner = get_runner(settings)
    test_runner = TestRunner(interactive=False)
    failures = test_runner.run_tests(["NEMO_reports/tests"])
    sys.exit(bool(failures))
